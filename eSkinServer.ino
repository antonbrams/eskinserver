
#include "./config.h"
#include "./Buffer.h"
#include "./Client.h"
#include "./messenger.h"

void setup() {
	#ifdef DEV
		Serial.begin(9600);
		Serial.println("\nStarting a Sketch...");
	#endif
	// regular initialisation
	Wire.begin();
	messenger::setup();
}

void loop() {
	
	int id = client::getId();
	if (clients[id].available())
		// clients[id].pulse();
		bufferOut.push(id);
	
	messenger::loop();
	
	if (messenger::available()) {
		int id = messenger::received[0];
		bufferIn.push(id);
		// clients[id].vibrate(messenger::received[1]);
	}
	
	int pulseId = bufferIn.pop();
	if (pulseId > -1) clients[pulseId].pulse();
	
	
	// animate vibration
	clients[id].loop();
}
