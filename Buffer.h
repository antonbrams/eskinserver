
class Buffer {
	
		int head = 0;
		int tail = 0;
		int updatedClientsBuffer[CLIENT_NUMBER] = {};
		bool updatedClientsRegister[CLIENT_NUMBER] = {};
	
	public:
		
		Buffer () {
			// fill with falses
			for (int i = 0; i < CLIENT_NUMBER; i ++) {
				updatedClientsBuffer[i]   = -1;
				updatedClientsRegister[i] = false;
			}
		}
		
		void push (int id) {
			// check for existance and if head is in the range
			if (!updatedClientsRegister[id]) {
				updatedClientsRegister[id] = true;
				updatedClientsBuffer[head] = id;
				head = (head + 1) % CLIENT_NUMBER;
			}
		}
		
		int pop () {
			if (head != tail) {
				// get the first id
				int id = updatedClientsBuffer[tail];
				tail = (tail + 1) % CLIENT_NUMBER;
				// mark as non existent
				updatedClientsRegister[id] = false;
				// out
				return id;
			} return -1;
		}
		
		bool available () {
			return head != tail;
		}
		
} bufferOut, bufferIn;
