
#include <Wire.h>

namespace client {
	unsigned long timeAsync;
	int id = 0;
	
	int getId () {
		id = (id + 1) % CLIENT_NUMBER;
		return id;
	}
}

class Client {
	public:
		const int address;
		int valueOld = 0;
		int valueHF  = 0;
		unsigned long timeChanged;
		unsigned long timeReceivedPulse;
		bool receivedPulseDebouncer = false;

		Client (int _address) : address(_address) {}

		int read() {
			int out = 0;
			Wire.requestFrom(address, 1);
			if (Wire.available() == 1)
				out = Wire.read();
			return out;
		}

		bool available() {
			// get new value
			int valueRaw = read();
			valueHF += (valueRaw - valueHF) * .1;
			// compare
			if (
				valueOld != valueHF &&
				millis() > timeChanged + RECEIVE_BREAK
			) {
				valueOld = valueHF;
				timeChanged = millis();
				return true;
			} else
				return false;
		}

		int getValue () {
			return valueHF;
		}
		
		void pulse () {
			timeReceivedPulse = millis();
			receivedPulseDebouncer = true;
			vibrate(255);
			vibrate(255);
		}
		
		void loop () {
			if (
				receivedPulseDebouncer &&
				millis() > timeReceivedPulse + RECEIVE_BREAK
			) {
				receivedPulseDebouncer = false;
				vibrate(0);
				vibrate(0);
			}
		}
		
		void vibrate(int value) {
			Wire.beginTransmission(address);
			Wire.write(value);
			Wire.endTransmission();
		}
		
} clients [CLIENT_NUMBER] = {ADDRESSES};
