
#define Antenna Serial1

namespace messenger {

	String buffer_received;
	int received[3] = {};

	void setup () {
		Antenna.begin(9600);
	}
	
	void setAT () {
		if (Serial.available())
			Antenna.write(Serial.read());
		if (Antenna.available())
			Serial.write(Antenna.read());
	}

	// send
	void send (int id, int value) {
		String toSend = 
			String(id) +","+ 
			String(value) +","+ 
			String((id + value) % 255);
		for (int i = 0; i < toSend.length(); i ++)
			Antenna.write(toSend.charAt(i));
		Antenna.write('\n');
	}
	
	// receive
	bool newMessage = false;
	void parse (String buffer) {
		String digitAccumulator;
		int counter = 0;
		int length = buffer.length();
		for (int i = 0; i < length; i ++) {
			char digit = buffer.charAt(i);
			if (digit != ',') digitAccumulator += digit;
			if (digit == ',' || i == length - 1) {
				received[counter] = digitAccumulator.toInt();
				digitAccumulator = "";
			}
			if (digit == ',') counter ++;
		}
		bool checkLength = counter == 2;
		bool checkSum    = (received[0] + received[1]) % 255 == received[2];
		bool checkId     = received[0] < CLIENT_NUMBER;
		bool checkValue  = 0 <= received[1] && received[1] <= 255;
		if (checkLength && checkSum && checkId && checkValue)
			newMessage = true;
	}

	void receive () {
		while (Antenna.available()) {
			char income = Antenna.read();
			if (income != '\n') {
				buffer_received += income;
			} else {
				parse(buffer_received);
				buffer_received = "";
			}
		}
	}

	bool available () {
		bool temp = newMessage;
		newMessage = false;
		return temp;
	}
	
	void receiveSimple () {
		if (Antenna.available()) {
			int income = Antenna.read();
			bool checkId = income < CLIENT_NUMBER;
			if (checkId) {
				received[0] = income;
				newMessage = true;
			}
		}
	}
	
	void loop () {
		if (bufferOut.available())
			if (millis() % COMMUNICATION_INTERVAL == 0) {
				int id = bufferOut.pop();
				// send(id, client[id].getValue());
				Antenna.write(id);
			}
		receiveSimple();
		// receive();
		
	}
}
